### Description

[The issue title is the title of the proposed article. Add the description here, unless the title is self-explaining. If possible, link to anything similar to what you want.]

### Outlines

[If you need specific outlines, please add them here. Otherwise, remove this whole section.]

----

### Attention

Before submitting a proposal to write about this topic:

- Read through the [Community Writers Program](https://about.gitlab.com/handbook/product/technical-writing/community-writers/).
- Read through the [Terms and Conditions](https://about.gitlab.com/handbook/product/technical-writing/community-writers/terms-and-conditions/).
- Please advise, for this program you'll write a [technical article](https://docs.gitlab.com/ce/development/writing_documentation.html#technical-articles) according to this [Writing Method](https://about.gitlab.com/handbook/product/technical-writing/#writing-method).
